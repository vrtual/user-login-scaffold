# UserLogin/userlogin/scaffolds/__init__.py

from pyramid.scaffolds import PyramidTemplate

class UserLoginTemplate(PyramidTemplate):
    _template_dir = 'userlogin_scaffold'
    summary = 'User login extension'
