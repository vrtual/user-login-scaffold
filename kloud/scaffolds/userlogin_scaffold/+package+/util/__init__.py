import os
import random
import string

def idGenerator(length=12, hex=True):
    """ Return a random string of lowercase letters and digits.
    
    Not intended to be thoroughly unique or secure. Relies on python's random module which claims the following about initialisation:
    
    Current system time is also used to initialize the generator when the module is first imported.
    If randomness sources are provided by the operating system, they are used instead of the system time
    (see the os.urandom() function for details on availability).
    """
    if hex:
        data = 'abcdef' + string.digits
    else:
        data = string.ascii_lowercase + string.digits
    return ''.join(random.choice(data) for x in range(length))

def isSandbox():
    isSandbox = False
    if 'SANDBOX' in os.environ:
        isSandbox = int(os.environ['SANDBOX'])
    return isSandbox