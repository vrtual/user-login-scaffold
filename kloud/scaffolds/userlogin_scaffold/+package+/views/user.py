import time
import re
import logging
import formencode
from pyramid_simpleform import Form
from pyramid_simpleform.renderers import FormRenderer
from pyramid.security import forget, remember, authenticated_userid
from pyramid.view import view_config
from pyramid.url import route_path
from pyramid.httpexceptions import HTTPFound
from pyramid.exceptions import Forbidden
from ..model.user import User

class SecurePassword(formencode.validators.FancyValidator):
    min = 3
    non_letter = 1
    letter_regex = re.compile(r'[a-zA-Z]')
    
    messages = {
        'too_few': 'Your password must be longer than %(min)i '
        'characters long',
        'non_letter': 'You must include at least %(non_letter)i '
        'characters in your password',
        }
    
    def _convert_to_python(self, value, state):
        # _convert_to_python gets run before _validate_python.
        # Here we strip whitespace off the password, because leading
        # and trailing whitespace in a password is too elite.
        print value
        return value.strip()
    
    def _validate_python(self, value, state):
        print self.min
        if len(value) < self.min:
            raise formencode.Invalid(self.message("too_few", state,
                                       min=self.min),
                                       value, state)
        non_letters = self.letter_regex.sub('', value)
        if len(non_letters) < self.non_letter:
            raise formencode.Invalid(self.message("non_letter", state,
                                       non_letter=self.non_letter),
                                       value, state)
    
class RegistrationSchema(formencode.Schema):
    allow_extra_fields = True
#    password = SecurePassword(min=7)
    password = formencode.validators.String(not_empty=True,min=7,max=128)
    confirm_password = formencode.validators.String(not_empty=True)
    email = formencode.validators.Email(resolve_domain=False)
    name = formencode.validators.String(not_empty=True, min=1, max=128)
#    confirm_password = formencode.validators.String(not_empty=True)
    chained_validators = [
        formencode.validators.FieldsMatch('password', 'confirm_password')
    ]
    
class ChangePasswordSchema(formencode.Schema):
    oldPassword = formencode.validators.String(not_empty=True)
    newPassword = formencode.validators.String(not_empty=True, min=7, max=128)
    confirmPassword = formencode.validators.String(not_empty=True)
    chained_validators = [
        formencode.validators.FieldsMatch('newPassword', 'confirmPassword')
    ]
    
class ResetPasswordEmailSchema(formencode.Schema):
    """ Take the email address you want to reset the password for
    
    """
    allow_extra_fields = True
    email = formencode.validators.Email(resolve_domain=False)
    
class ResetPasswordSchema(formencode.Schema):
    """ Take the two password fields for resetting the account.
    
    """
    allow_extra_fields = True
    password = formencode.validators.PlainText(not_empty=True)
    confirm_password = SecurePassword()
    chained_validators = [ formencode.validators.FieldsMatch('password', 'confirm_password') ]    
        
class UserViews( object ):
    
    def __init__(self, request):
        self.request = request

    def __getSessionKey(self):
        return self.request.session['_csrft_']
    
    def __logoutUser(self):
        self.request.session.invalidate()
        self.request.session.delete()
        headers = forget(self.request) 
        return headers
    
    @view_config( route_name="logout" )
    def logout( self ):
        logging.info("Logged out %s (%s)"%(authenticated_userid(self.request), self.request.client_addr))
        headers = self.__logoutUser()
        self.request.session.flash(u'Logged out successfully.')
        return HTTPFound(location=route_path('login', self.request), headers=headers)
    
    @view_config( route_name='register', renderer='register.mako', permission='view' )
    def registerUser( self ):
        form = Form(self.request, schema=RegistrationSchema)
        message = None
        if 'form.submitted' in self.request.POST and form.validate():
            email = form.data['email']
            user = User.createUser( form.data['name'], email, form.data['password'])
            if user:
                headers = remember(self.request, user.email)
                redirect_url = self.request.route_path('home')
                return HTTPFound(location=redirect_url, headers=headers)
            else:
                message = 'Email address is already registered'
        return { 'message' : message, 'form': FormRenderer(form) }
    
    @view_config( route_name='resendRegistration', renderer='registered.mako', permission='view' )
    def resendRegistration(self):
        if 'email' in self.request.params:
            user = User.getUser(self.request.params['email'])
            if user and not user.registrationConfirmed:
                user.regenerateRegistrationCode()
                url = self.request.route_url('confirmRegistration', _query={'email':user.email, 'confirm':user.registrationCode} )
#                sendRegistrationConfirmation(user, url)
                status = 'resent'
            elif user.registrationConfirmed:
                status = 'registered'
            else:
                status = 'invalid'
        logging.info("Resending registration confirmation to %s status '%s' (%s)"%(self.request.params['email'], status, self.request.client_addr))
        return {'status' : status}
    
    @view_config( route_name='pendingRegistration', renderer='registered.mako', permission='view' )
    def pendingRegistration(self):
        return { 'status' : 'waiting' } 

    @view_config( route_name='confirmRegistration', renderer='registered.mako', permission='view' )
    def confirmRegistration(self):
        if 'confirm' in self.request.params and 'email' in self.request.params:
            user = User.getUser(self.request.params['email'])
            logging.info("Registration confirmation from %s (%s)"%(self.request.params['email'], self.request.client_addr))
            if user.registrationConfirmed: # in case they go back and reload
                return { 'status' : 'confirmed' }
            if user and user.registrationCode == self.request.params['confirm']:
                user.confirmRegistration()
                return { 'status' : 'confirmed' }
        return { 'status' : 'unconfirmed'}
   
    @view_config( route_name='resetPassword', renderer='resetPassword.mako', permission='view' )
    def resetPasswordRequest(self):
        # Step 2: confirmation received
        if 'confirm' in self.request.params and 'email' in self.request.params:
            msg = "Reset password confirmation for %s on %s (%s)"%(self.request.params['email'], self.request.host, self.request.client_addr)
            logging.info(msg)
            user = User.getUser(self.request.params['email'])
            if user and user.registrationCode == self.request.params['confirm']:        
                logging.info("Reset password confirmation code matches %s for  %s (%s)"%(user.registrationCode, self.request.params['email'], self.request.client_addr))
                form = Form(self.request, schema=ResetPasswordSchema)
                return { 'status' : 'passwords', 'form' : FormRenderer(form), 'confirmation' : user.registrationCode, 'email' : user.email }
            else:
                logging.warn("Reset password confirmation code FAILED %s vs %s for  %s (%s)"%(self.request.params['confirm'], user.registrationCode, self.request.params['email'], self.request.client_addr))
                return { 'status' : 'error' }

        # Step 1: get an email address to reset the password for
        if 'form.submitted' in self.request.POST:
            form = Form(self.request, schema=ResetPasswordEmailSchema)
            if form.validate():
                time.sleep(2) # slow down attacks        
                user = User.getUser(form.data['email'])
                # generates as different email depending on whether we have an account called that
                logging.info("Reset password requested for %s (%s)"%(self.request.params['email'], self.request.client_addr))
                if user:
                    user.regenerateRegistrationCode()
#                    sendPasswordReset(form.data['email'], self.request.route_url('resetPassword', _query={'email': form.data['email'], 'confirm':user.registrationCode} ))
                else:
                    pass
#                    sendPasswordReset(form.data['email'], None)
                return { 'status' : 'sent', 'form' : FormRenderer(form) }
            else:
                return { 'status' : 'send', 'form' : FormRenderer(form) }
 
        # Step 3: the reset password form coming back
        elif 'form2.submitted' in self.request.POST:
            form = Form(self.request, schema=ResetPasswordSchema)
            if form.validate():
                time.sleep(1)
                logging.info("Reset password submitted for %s (%s)"%(form.data['email'], self.request.client_addr))
                user = User.getUser(form.data['email'])
                if not user:
                    return { 'status' : 'error' }
                conf = form.data['confirmation']
                if user.registrationCode != conf:
                    return { 'status' : 'error' }
                # accept the new password
                logging.info( "Password reset for %s (%s)"%(user.email, self.request.client_addr))
                user.resetPassword(form.data['password'])
                return { 'status' : 'done' }
            else:
                if 'confirmation' in form.data and 'email' in form.data:
                    return { 'status' : 'passwords', 'form' : FormRenderer(form), 'confirmation' : form.data['confirmation'], 'email' : form.data['email'] }
                else:
                    return { 'status' : 'error' }
                
        else:
            form = Form(self.request, schema=ResetPasswordEmailSchema)
            return { 'form' : FormRenderer(form), 'status' : 'send' }

    @view_config( route_name="api", request_method="POST", match_param=['category=you', 'action=changePassword'], permission='post', renderer="json")
    def changePassword(self):
        user = User.getUser(authenticated_userid(self.request))
        if not user:
            raise Forbidden()        
        errors = {}
        schema = ChangePasswordSchema()
        status = 'errors'
        try:
            schema.to_python(self.request.json_body)
            checkUser = User.getUser(user.email, self.request.json_body['oldPassword'])
            if not checkUser or checkUser.objectId != user.objectId:
                errors = { 'oldPassword' : "Old password is incorrect" }
            else:
                status = 'OK'
                user.resetPassword(self.request.json_body['newPassword'])
        except formencode.Invalid, e:
            errors = e.unpack_errors()
        
        return { 'status' : status, 'errors' : errors }