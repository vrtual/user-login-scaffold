from pyramid.view import view_config
from pyramid.exceptions import Forbidden

from ..model.user import User
from .. import VERSION

class SomeViews(object):
    def __init__(self, request):
        self.request = request
        
    @view_config( route_name="home", permission='post', renderer="home.mako")
    def home(self):
        user = User.getUser(self.request.authenticated_userid)
        if not user:
            raise Forbidden()

        return { 
                'user' : user,
                'version' : VERSION,
                }
        