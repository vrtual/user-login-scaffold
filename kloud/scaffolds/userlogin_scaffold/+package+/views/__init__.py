import logging, pprint, datetime, time
import formencode
import re
from pyramid_simpleform import Form
from pyramid_simpleform.renderers import FormRenderer
from pyramid.view import view_config, notfound_view_config, forbidden_view_config
from pyramid.httpexceptions import HTTPFound, HTTPBadRequest
from pyramid.security import remember, forget
from ..model.user import User 
from .. import VERSION

def getEmailFromUserString(userString):
    """Extract an email address from the given string
    
    """
    matches = re.findall('[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}', userString.upper() )
    if matches:
        return matches[0].lower()
    return None

def failResponse(msg):
    response = HTTPBadRequest()
    response.text = unicode(msg)
    return response 

def wrapResponse(e):
    """ Helper method to wrap an exception for returning over http
    
    """
    response = HTTPBadRequest()
    response.text = unicode(e)
    return response

@forbidden_view_config(renderer="badPage.mako")
def forbiddenView(request):
    """ This is called whenever HTTPForbidden is generated or pyramid.exceptions.forbidden is raised.
    
    This could be because they're not logged in, in which case be polite and take them to the login page.
    Otherwise they're trying to access a document they shouldn't be able to. A little rudeness is required.
    """
    if not request.authenticated_userid:
        return HTTPFound( request.route_url("login"))
    logging.warn("Forbidden access to %s by %s (%s)"%(request.url, request.authenticated_userid, request.client_addr))
    return { 'msg' : 'Access to this page is not permitted' }

class LoginSchema( formencode.Schema ):
    allow_extra_fields = True
    password = formencode.validators.String(not_empty=True)
    email = formencode.validators.Email(resolve_domain=False)
    
#@view_config(context=TimeoutException, renderer="login.mako")
#def sessionTimeout(exc, request):
#    """ We raise a TimeoutException (see beany.model.user) and take the user back to the login screen with a message saying we logged them out.
#    
#    Apparently I should be able to add the 'context' to the arg to the 'login' route below, but it doesn't seem to work.
#    Hence we have two equivalent methods and this is now all in this module and not 'user' view.
#    """
#    logging.info( "User %s session timed out (%s)"%(request.authenticated_userid, request.client_addr))
#    request.session.invalidate()
#    request.session.delete()
#    headers = forget(request) 
#    return HTTPFound(location=request.route_path('login', _query={'timeout':''}), headers=headers)
    
@view_config( route_name="login", renderer="login.mako")
def login(request):
    form = Form(request, schema=LoginSchema)
    now = datetime.datetime.now()
    formRenderer = FormRenderer(form)
        
    data = { 'form' : formRenderer, 'version' : VERSION, 'status' : 0, 'msg' : '' }
#    if 'timeout' in request.params:
#        data['status'] = User.SESSION_TIMEOUT
        
    if request.method == 'POST':
        token = request.POST.get("_csrf")
        if token is None or token != request.session.get_csrf_token():
            data['status'] = User.STATUS_INVALID_LOGIN
            return data
    if 'form.submitted' in request.POST:
        if form.validate() and 'email' in form.data:
            data['email'] = form.data['email']
            user = User.getUser(form.data['email'], form.data['password'])
            if user:
                headers = remember(request, user.email)
                redirect_url = request.route_path('home')
                user.resetLoginTimer()
                msg = "%s logged into %s from %s"%(data['email'], request.host, request.client_addr)
                logging.info( msg )
                return HTTPFound(location=redirect_url, headers=headers)
            else:
                time.sleep(1) # slow down attacks
                logging.warn("Attempted login from %s (%s)"%(data['email'], request.client_addr))
                data['status'] = User.STATUS_USERNAME_PASSWORD_MISMATCH
                msg = 'Sorry - the username or password is wrong'
        else:
            time.sleep(1) # slow down attacks
            logging.warn("Invalid login info (%s)"%request.client_addr)
            data['status'] = User.STATUS_INVALID_LOGIN
            msg = "There's something wrong with that login"
    return data
