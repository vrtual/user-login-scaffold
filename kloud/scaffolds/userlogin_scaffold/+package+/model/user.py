# module for managing user login 
#
import pprint
import string
import logging
import cryptacular.bcrypt
import random
import datetime
import formencode
from bson.objectid import ObjectId

from mongocfg import MongoCfg
from ..util import idGenerator, isSandbox

#from persistent import Persistent
#from persistent.mapping import PersistentMapping

crypt = cryptacular.bcrypt.BCRYPTPasswordManager()

#class TimeoutException(Exception):
#    pass

class UserDB( object ):
    """ Low-level access to the user database.
    
    """
    def __init__(self):
        pass
 
    @property
    def usersCollection(self):
        return MongoCfg.getDB().users
    
    def addUser(self, user ):
        return self.usersCollection.insert( user.toDict() )

    def updateUser(self, user):
        self.usersCollection.update( { '_id' : user.objectId }, {'$set':user.toDict()} )

    def getUserByEmail(self, email ):
        return self.usersCollection.find_one( { 'email' : email.lower() } )
    
    def getUserById(self, userId ):
        return self.usersCollection.find_one( { '_id' : userId } )

    def deleteUser(self, userId ):
        return self.usersCollection.remove( { '_id' : userId } )
    
    def deleteUserByEmail(self, email ):
        return self.usersCollection.remove( { 'email' : email.lower() } )

    def updateField(self, user, field, value):
        return self.usersCollection.update({'_id':user.objectId}, {'$set': {field: value }})
    
    @classmethod
    def getGroups(cls, email, request):
        groups = MongoCfg.getDB().users.find_one( {'email' : email.lower() }, { 'groups' : 1 } )
        if groups and 'groups' in groups:
            return groups['groups']
        return []

    @classmethod
    def getUserSearchData(cls):
	""" For feeding user data into the javascript table helper.

	"""
        return [ a for a in MongoCfg.getDB().users.find( {}, 
                { 'email' : 1, 'name' : 1, 'accountCreated' : 1, 'lastAccess' : 1 } ) ]
 
class User( object ):
    USER_DB = UserDB()
    STATUS_NONE = -1
    STATUS_OK = 0
    STATUS_SUSPENDED = 20
    STATUS_REGISTRATION_NOT_CONFIRMED = 30
    STATUS_USERNAME_PASSWORD_MISMATCH = 40
    STATUS_INVALID_LOGIN = 50
    
    def __init__(self, name, email, password, objectId=None ): 
        self.name = name
        self.password = password 
        self.email = email
        self.objectId = objectId
        self.groups = []

    def toDict(self):
        d = { 'name' : self.name, 'password' : self.password, 'email' : self.email.lower() }
        return d

    def fromDict(self, record ):
        fields = { 'groups' }
        for f in fields:
            if f in record:
                self.__setattr__(f, record[f])

    def persist( self ):
        self.USER_DB.updateUser( self )
 
    def confirmRegistration(self):
        self.registrationConfirmed = True
        self.persist()

    def resetLoginTimer(self):
        self.lastAccess = datetime.datetime.now()
        self.USER_DB.updateField(self, 'lastAccess', self.lastAccess)
        
    def regenerateRegistrationCode(self):
        self.registrationCode = idGenerator()
        self.USER_DB.updateField(self, 'registrationCode', self.registrationCode)
        
    def resetPassword(self, password):
        self.password = unicode( crypt.encode( password ) )
        self.USER_DB.updateUser(self)
        
    @classmethod
    def getUser(cls, email, password=None ):
        if not email:
            return None
        userData = UserDB().getUserByEmail( email )
        if not userData:
            logging.warn( 'User %s not found'%email.lower() );
            return None
        user = User( userData['name'], userData['email'].lower(), userData['password'], userData['_id'] )
        user.fromDict(userData)
        
        if not password:
            return user
        if crypt.check( userData['password'], password):
            return user
        else:
            logging.warn('Password failed')
            return None

    @classmethod
    def getUserById(cls, userId ):
        userData = UserDB().getUserById(userId)
        if userData:
            user = User( userData['name'], userData['email'].lower(), userData['password'], userData['_id'] )
            user.fromDict( userData )
            return user
        return None
 
    @classmethod
    def createUser(cls, name, email, password):
        user = cls.USER_DB.getUserByEmail( email )
        if user:
            logging.error( "Tried to create existing user %s"%email)
            return None
        user = User( name, email, unicode( crypt.encode( password ) ) )
        user.registrationCode = idGenerator()
        user.accountCreated = datetime.datetime.now()
        user.objectId = cls.USER_DB.addUser( user ) # this needs to be last! It persists the record
        return user
    
    @classmethod
    def deleteUser(cls, userId, full=False ):
        if full: # remove all traces of the user from the beany collections
            db = MongoCfg.getDB()
            db.users.remove({ '_id' : userId})
            db.personal.remove({'user' : userId})
            # remove linked organisations with only them linked
        cls.USER_DB.deleteUser(userId)
