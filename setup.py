import os
from setuptools import setup, find_packages

# Utility function to read the README file.
# Used for the long_description.  It's nice, because now 1) we have a top level
# README file and 2) it's easier to type in the README file than to put a raw
# string in below ...
def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup(
    name = "user_login_extension",
    version = "0.0.15",
    author = "John Curtis",
    author_email = "john@kloud.co.nz",
    description = ("A scaffold for setting up user logins in Pyramid"),
    license = "BSD",
    keywords = "user login pyramid",
#    url = "http://packages.python.org/an_example_pypi_project",
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    long_description=read('README'),
    classifiers=[
        "Development Status :: 1 - Alpha",
        "Topic :: Scaffolds",
        "License :: BSD License",
    ],
    entry_points = """\
        [pyramid.scaffold]
        userlogin=kloud.scaffolds:UserLoginTemplate
      """
)
